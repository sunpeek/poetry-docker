ARG PYTHON_VERSION=python:3.8-slim
FROM python:$PYTHON_VERSION

RUN python -m pip install poetry