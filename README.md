# Poetry Docker
This project contains the Dockerfile and build config for creating a Docker image based on python, with the poetry 
environment and build management tool pre-installed.

## Build
This repository has a CI pipeline configured to be run manually. To use go to 
https://gitlab.com/sunpeek/poetry-docker/-/pipelines/new and set a variable called `PYTHON_VERSION` to the tag of the 
base [python image to use](https://hub.docker.com/_/python/) (e.g. `3.8-slim`)
